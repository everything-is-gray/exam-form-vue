Vue.component('identification', {
  template:`
    <div id="identificaiton">
      <div class="row">
        <div class="col-8 full-fill-in">
          Name: <input type="text"></input>
        </div>
        <div class="col-4 full-fill-in">
          Date: <input type="text"></input>
        </div>
      </div>
      <div class="row">
        <div class="col-4 full-fill-in">
          DOB: <input type="text"></input>
        </div>
        <div class="col-2 full-fill-in">
          Age: <input type="text"></input>
        </div>
        <div class="col-6 full-fill-in">
          Phone: <input type="text"></input>
        </div>
      </div>
      <div class="row">
        <div class="col-12 full-fill-in">
          Address: <input type="text"></input>
        </div>
      </div>
    </div>
  `
});
Vue.component('pre-exam', {
  template:`
  <div id="pre-exam">
    <div class="row">
      <div class="col-12 text-center">
        <strong>Pre-Exam Instructions</strong><br />
        Please follow all checked instructions prior to your appointment.  Contact your provider with any questions.
      </div>
    </div>
    <div class="row no-row-sep">
      <div class="col-6">
        <input type="checkbox"/> Body Hair Removal
      </div>
      <div class="col-6 full-fill-in">
        Regions: <input type="text"></input>
      </div>
    </div>
    <div class="row no-row-sep">
      <div class="col-6">
        <input type="checkbox"/> Enema
      </div>
      <div class="col-4 full-fill-in">
        Qty: <input type="text"></input>
      </div>
      <div class="col-2">
        <input type="checkbox"/> Till Clean
      </div>
    </div>
    <div class="row no-row-sep">
      <div class="col-6">
        <input type="checkbox" /> Chastity Device
      </div>
      <div class="col-6 full-fill-in">
        Duration Prior to Appt: <input type="text"></input>
      </div>
    </div>
    <div class="row no-row-sep">
      <div class="col-6">
        <input type="checkbox" /> Refrain from Orgasms
      </div>
      <div class="col-6 full-fill-in">
        Duration Prior to Appt: <input type="text"></input>
      </div>
    </div>
    <div class="row no-row-sep">
      <div class="col-6">
        <input type="checkbox" /> Orgasm Edges
      </div>
      <div class="col-6 full-fill-in">
        Quantity: <input type="text"></input>
      </div>
    </div>
  </div>
  `,
  props: {
    bodyHair: {
      type: Boolean,
      default: true
    },
    enema: {
      type: Boolean,
      default: true
    },
    chastity: {
      type: Boolean,
      default: true
    },
    denial: {
      type: Boolean,
      default: true
    },
    edges: {
      type: Boolean,
      default: true
    }
  }
});
Vue.component('general-exam', {
  template: `
    <div id="general-exam">
      <div class="row">
        <div class="col-3 d-flex align-items-center" style="justify-content:space-between;">
          <span>Height:</span><span><input type="text" size="1" maxlength="1" style="text-align:right;margin-right:0.2em;">ft<input type="text" size="2" maxlength="2" style="text-align:right;margin-right:0.2em;">in</span>
        </div>
        <div class="col-6 d-flex align-items-center" style="border-style:none;">
          Weight:<input type="text" size="3" maxlength="3" style="text-align:right;margin-right:0.2em;">lbs
        </div>
        <div class="col-3">
          <div class="row" style="border-style:none;">
            <div class="col-7" style="border-style:none;"><input type="checkbox" /> Clothed</div>
            <div class="col-5" style="border-style:none;"><input type="checkbox" /> Nude</div>
          </div>
          <div class="row" style="border-style:none;">
            <div class="col-12" style="border-style:none;"><input type="checkbox" /> Undergarments/Gown</div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-3 full-fill-in d-flex align-items-center">
          BP:<input type="text" size="3" maxlength="3" style="text-align:right;"></input>/<input type="text" size="3" maxlength="3"></input>
        </div>
        <div class="col-5 d-flex align-items-center full-fill-in" style="justify-content:space-between;">
          Pulse:<input type="text" maxlength="3">
          <span>
            <div class="row" style="border-style:none;">
              <div class="col-12" style="border-style:none;"><input type="checkbox" /> Reg</div>
            </div>
            <div class="row" style="border-style:none;">
              <div class="col-12" style="border-style:none;"><input type="checkbox" /> Irr</div>
            </div>
          </span>
        </div>
        <div class="col-4 d-flex align-items-center full-fill-in" style="justify-content:space-between;">
          Temp:<input type="text" maxlength="3">
          <span>
            <div class="row" style="border-style:none;">
              <div class="col-12" style="border-style:none;"><input type="checkbox" /> Oral</div>
            </div>
            <div class="row" style="border-style:none;">
              <div class="col-12" style="border-style:none;"><input type="checkbox" /> Rectal</div>
            </div>
          </span>
        </div>
      </div>
    </div>
  `
});
Vue.component('physical-item', {
  template:`
    <div class="row">
      <div class="col-4">
        {{desc}}
      </div>
      <div class="col-4">
        <div class="row" style="border-style:none;">
          <div class="col-6" style="border-style:none;">
            <input type="checkbox" /> Normal
          </div>
          <div class="col-6" style="border-style:none;">
            <input type="checkbox" /> Not Examined
          </div>
        </div>
        <div class="row" style="border-style:none;">
          <div class="col-12" style="border-style:none;">
            <input type="checkbox" /> Abnormal
          </div>
        </div>
      </div>
      <div class="col-4">
        Comments:
      </div>
    </div>
  `,
  props: ['desc']
});
Vue.component('physical-exam', {
  template:`
    <div id="physical-exam">
      <physical-item v-for="item in items" :key="item" :desc="item" class="physical-item"></physical-item>
    </div>
  `,
  data: function () {
    return {
      items: [
      "Appearance",
      "Posture",
      "Flexibility",
      "Neurological",
      "Head",
      "Ears/Eyes/None/Throat",
      "Lymph Nodes",
      "Neck",
      "Chest",
      "Abdomen",
      "Back",
      "Spine",
      "Buttocks/Anus",
      "Genitals",
      "Shoulders/Arms/Hands",
      "Hips/Legs/Feet",
      "Skin"
    ]}
  }
});
Vue.component('body-measurements', {
  template:`
  `
})


var App = new Vue({
  el: '#app',
  data: {
  }
})
